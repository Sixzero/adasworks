#include "mydatasetreader.h"

#include <math.h>
#include <iostream>
#include <fstream>
#include <cstring>

myDatasetReader::myDatasetReader()
{

}
// readMNIST.cc
// read MNIST data into double vector, OpenCV Mat, or Armadillo mat
// free to use this code for any purpose
// author : Eric Yuan
// my blog: http://eric-yuan.me/
// part of this code is stolen from http://compvisionlab.wordpress.com/


using namespace std;

int myDatasetReader::ReverseInt (int i)
{
    unsigned char ch1, ch2, ch3, ch4;
    ch1 = i & 255;
    ch2 = (i >> 8) & 255;
    ch3 = (i >> 16) & 255;
    ch4 = (i >> 24) & 255;
    return((int) ch1 << 24) + ((int)ch2 << 16) + ((int)ch3 << 8) + ch4;
}

void myDatasetReader::read_Mnist_images(string filename, vector<vector<double> > &vec)
{
//    cout<<filename<<endl;
    ifstream file (filename, ios::binary);
    if (file.is_open())
    {
        int magic_number = 0;
        int number_of_images = 0;
        int n_rows = 0;
        int n_cols = 0;
        file.read((char*) &magic_number, sizeof(magic_number));
        magic_number = ReverseInt(magic_number);
        file.read((char*) &number_of_images,sizeof(number_of_images));
        number_of_images = ReverseInt(number_of_images);
        file.read((char*) &n_rows, sizeof(n_rows));
        n_rows = ReverseInt(n_rows);
        file.read((char*) &n_cols, sizeof(n_cols));
        n_cols = ReverseInt(n_cols);
        for(int i = 0; i < number_of_images; ++i)
        {
            vector<double> tp;
            for(int r = 0; r < n_rows; ++r)
            {
                for(int c = 0; c < n_cols; ++c)
                {
                    unsigned char temp = 0;
                    file.read((char*) &temp, sizeof(temp));
                    tp.push_back((double)temp/normalizationScale);
                }
            }
            vec.push_back(tp);
        }
    } else {

        cout<<"File open failed"<<endl;
        cout << "Error: " << strerror(errno) <<endl;
    }
}

void myDatasetReader::read_Mnist_Label(string filename, vector<double> &vec)
{
    ifstream file (filename, ios::binary);
    if (file.is_open())
    {
        int magic_number = 0;
        int number_of_images = 0;
        int n_rows = 0;
        int n_cols = 0;
        file.read((char*) &magic_number, sizeof(magic_number));
        magic_number = ReverseInt(magic_number);
        file.read((char*) &number_of_images,sizeof(number_of_images));
        number_of_images = ReverseInt(number_of_images);
        for(int i = 0; i < number_of_images; ++i)
        {
            unsigned char temp = 0;
            file.read((char*) &temp, sizeof(temp));
            vec.push_back( (double)temp );
        }
    }else {
        cout<<"File open failed"<<endl;
    }
}
#include <sstream>
template <typename T>
std::string to_string(T const& value) {
    stringstream sstr;
    sstr << value;
    return sstr.str();
}

void myDatasetReader::read_Mnist_train()
{
    string filename = "../mysolution/data/train-images.idx3-ubyte";
    string filename_labels = "../mysolution/data/train-labels.idx1-ubyte";
    read_Mnist(filename, filename_labels);
    return ;
}
void myDatasetReader::read_Mnist_test()
{
    string filename = "../mysolution/data/t10k-images.idx3-ubyte";
    string filename_labels = "../mysolution/data/t10k-labels.idx1-ubyte";
    read_Mnist(filename, filename_labels);
    return ;
}
void myDatasetReader::read_Mnist(std::string imgPath, std::string labelPath)
{
    vec_X.clear();
    //read MNIST image into double vector
    read_Mnist_images(imgPath, vec_X);
//    cout<<vec.size()<<endl;
//    cout<<vec[0].size()<<endl;

//    //Kiiratás ellenőrzés...
//    for (int i = 0; i<1; ++i) {
//        string asdf ;
//        for (auto & t : vec_X[i]) {
//            asdf += " " + to_string(t);
//        }
//        cout<<asdf<<endl;
//    }



    //read MNIST label into double vector
    vector<double> vec_l;
    read_Mnist_Label(labelPath, vec_l);
//    cout<<vec_l.size()<<endl;

    for (int i = 0; i<vec_l.size(); ++i) {
        vec_Y.push_back(vector<double>(10,0.));
        vec_Y[i][vec_l[i]] = 1.;
    }

//    //Kiiratás ellenőrzés...
//    for (int i = 0; i<50; ++i) {
//        string asdf = to_string(vec_l[i]);
//        for (auto & t : vec_Y[i]) {
//            asdf += " " + to_string(t);
//        }
//        cout<<asdf<<endl;
//    }


    return ;
}

#include <algorithm>    // std::random_shuffle
Eigen::MatrixXf myDatasetReader::getBatchImg(size_t batchSize)
{
    if (counterImage + batchSize> vec_X.size()) {
        counterImage = 0;
        std::srand ( 42 );
        std::random_shuffle ( vec_X.begin(), vec_X.end() );
        std::srand ( 42 );
        std::random_shuffle ( vec_Y.begin(), vec_Y.end() );
        epoch++;
    }
    std::vector<std::vector<double> > myslice = slice(vec_X, counterImage , counterImage + batchSize );
    counterImage += batchSize;

    Eigen::MatrixXf asd(myslice.size(),myslice[0].size());
    for (int i = 0 ; i<myslice.size(); ++i)
        for (int j = 0 ; j<myslice[i].size(); ++j) {
            asd(i,j) = myslice[i][j];
        }
    return asd;
}

Eigen::MatrixXf myDatasetReader::getBatchLabel(size_t batchSize)
{

    if (counterLabel + batchSize> vec_Y.size()) {
        counterLabel = 0;
    }
    std::vector<std::vector<double> > myslice = slice(vec_Y, counterLabel , counterLabel + batchSize);
    counterLabel += batchSize;

    Eigen::MatrixXf asd(myslice.size(),myslice[0].size());

    for (int i = 0 ; i<myslice.size(); ++i)
        for (int j = 0 ; j<myslice[i].size(); ++j) {
            asd(i,j) = myslice[i][j];
        }
    return asd;
}
