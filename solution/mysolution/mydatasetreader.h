#ifndef MYDATASETREADER_H
#define MYDATASETREADER_H

#include <vector>
#include <string>
#include <Eigen/Dense>

class myDatasetReader
{
    size_t counterImage{0};
    size_t counterLabel{0};
    double normalizationScale{2500};
public:
    size_t epoch{0};

    std::vector<std::vector<double> > vec_X;
    std::vector<std::vector<double> > vec_Y;
    myDatasetReader();
    void read_Mnist(std::__cxx11::string imgPath, std::__cxx11::string labelPath);
    Eigen::MatrixXf getBatchImg(size_t batchSize);
    Eigen::MatrixXf getBatchLabel(size_t batchSize);
    void read_Mnist_train();
    void read_Mnist_test();
private:
    int ReverseInt(int i);
    void read_Mnist_Label(std::string filename, std::vector<double> &vec);
    void read_Mnist_images(std::string filename, std::vector<std::vector<double> > &vec);
    std::vector<std::vector<double> > slice(const std::vector<std::vector<double> > &v, int start = 0, int end = -1) {
        int oldlen = v.size();
        int newlen;

        if (end == -1 or end >= oldlen){
            newlen = oldlen-start;
        } else {
            newlen = end-start;
        }

        std::vector<std::vector<double> > nv;

        for (int i=0; i<newlen; i++) {
            nv.push_back(v[start+i]);
        }
        return nv;
    }
};

#endif // MYDATASETREADER_H
