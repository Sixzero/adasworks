#ifndef MYMATRIX_H
#define MYMATRIX_H

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <vector>

using namespace std;

class MyMatrix {
    vector<vector<int> > t;
    size_t rowCount, colCount;
public:
    MyMatrix (size_t r, size_t c) : rowCount(r), colCount(c) {
        t.resize(rowCount);
        for(size_t i = 0; i < rowCount; ++i)
            t[i].resize(colCount);

        for(size_t i=0; i<rowCount; ++i) {
            for(size_t j=0; j<colCount; ++j)
            {
               t[i][j]=abs(rand())>>28; // [0, 8]
            }
        }
    }

    MyMatrix  operator*(const MyMatrix& rhs)  {
        size_t s_common;
        if (rhs.rowCount != this->colCount) {
            cout << "Nagy a baj" << endl;
            // ASSERT
        }
        s_common = rhs.rowCount;
        size_t r = this->rowCount, c = rhs.colCount;
        MyMatrix  result{r, c};

        for (size_t i=0; i<r; i++) {
            for (size_t j=0; j<c; j++) {
                for (size_t n=0; n<s_common; n++) {
                    result.t[i][j] += this->t[i][n] * rhs.t[n][j];
                }
            }
        }
        return result;

    }
    void print() {
        cout << "Matrix: " << endl;
        for(size_t i=0; i<rowCount; ++i)
            for(size_t j=0; j<colCount; ++j)
            {
                cout << setw(5) << t[i][j];
                if(j==colCount-1)
                    cout << endl;
            }
    }

};


#endif // MYMATRIX_H
