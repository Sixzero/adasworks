#include <QDebug>

#include "mylayers/mymullayer.h"
#include "mylayers/myaddbiaslayer.h"
#include "mylayers/myinputlayer.h"
#include "mylayers/mysigmoidlayer.h"
#include "mylayers/myrelulayer.h"
#include "mylayers/myoutputlayer.h"
#include "mymodelcontainer.h"
#include "mydatasetreader.h"

#include <cstdlib>
#include <iomanip>
#include <iostream>

#include "justforfun.h"

using namespace std;

int main(int argc, char *argv[])
{
//    cout<<"Használt szálak száma: "<<Eigen::nbThreads( )<<endl;
    cout << "Reading train data:" << endl;
    myDatasetReader mnist_train;
    mnist_train.read_Mnist_train();
    cout << "Reading test data:" << endl;
    myDatasetReader mnist_test;
    mnist_test.read_Mnist_test();


    cout << "Creating model arhitecture:" << endl;
    size_t batchSize = 64;
    size_t inputSize= 28*28;
    size_t outputSize= 10;
    MyModelContainer myModel;
    // Creating input output variables for later use...
    MyInputLayer* layerX = new MyInputLayer{batchSize, inputSize};
    MyOutputLayer* layerY = new MyOutputLayer{batchSize, outputSize};

    // The arhitecture...
    size_t l1Size=480, l2Size=100, l3Size=25, l4Size=10;
    cout<< l1Size <<" "<<l2Size <<" "<< l3Size<<" "<< l4Size<<endl;
//    size_t l1Size=60, l2Size=20;
    myModel.myLayers.push_back(layerX);
    myModel.myLayers.push_back(new MyMulLayer{inputSize, l1Size});
//    myModel.myLayers.push_back(new MyAddBiasLayer{batchSize, l1Size}); // ront az eredményeken...
    myModel.myLayers.push_back(new MyReluLayer{l1Size});
    myModel.myLayers.push_back(new MyMulLayer{l1Size, l2Size});
    myModel.myLayers.push_back(new MyReluLayer{l2Size});
//    myModel.myLayers.push_back(new MyMulLayer{l2Size, outputSize});
    myModel.myLayers.push_back(new MyMulLayer{l2Size, l3Size});
    myModel.myLayers.push_back(new MyReluLayer{l3Size});
    myModel.myLayers.push_back(new MyMulLayer{l3Size, outputSize});
//    myModel.myLayers.push_back(new MyMulLayer{l3Size, l4Size});
//    myModel.myLayers.push_back(new MyReluLayer{l4Size});
//    myModel.myLayers.push_back(new MyMulLayer{l4Size, outputSize});
    myModel.myLayers.push_back(layerY);

    cout << std::setprecision(3)<< "Train calculation started:" << endl;
    Eigen::MatrixXf xt = mnist_test.getBatchImg(10000);
    Eigen::MatrixXf yt = mnist_test.getBatchLabel(10000);
    float lr = 0.05;
    for(int i = 0; i<10000;++i) {
        // Feed :)
        layerX->myInput = mnist_train.getBatchImg(batchSize);
        layerY->myRef = mnist_train.getBatchLabel(batchSize);

        myModel.run(lr);
//        cout<< setw(4)  <<i+1<<". step. Acc: "<< setw(4) <<layerY->getAccuracy() * 100<<"% Epoch: "<<setw(2)  <<mnist_train.epoch<<". Lr.: "<<setw(6)  <<lr<<" My Err: "<< setw(5) << layerY->myErr.sum() <<endl;
        if (lr>0.04)
            lr *= 0.999; // Exponenciális csökkentés...
        if (i%500==0) {
            layerX->myInput = xt;
            layerY->myRef = yt;
            myModel.predict();
            cout <<setw(2)  <<i/500<<". Acc: "<< setw(4) <<layerY->getAccuracy()*100<<"% Lr.: "<<setw(5)  <<lr<<endl;
        }
    }

    cout << "Test calculation started:" << endl;
    layerX->myInput = xt;
    layerY->myRef = yt;
    myModel.predict();
    cout <<"Acc: "<< setw(4) <<layerY->getAccuracy()*100<<"% Lr.: "<<setw(10)  <<lr<<endl;

//    // The wrong labelled pictures...
//    for(size_t i=0 ; i<layerY->misslabelledInputs.size() ; ++i) {
//        for(size_t j=0;j<2 && i+j<layerY->misslabelledInputs.size();++j){
//            cout<<"Predicted: " <<layerY->wrongLabels[i+j]<<endl;
//            printMatrixImg(xt.block(layerY->misslabelledInputs[i+j],0,1,28*28));
//        }
//        cin.get();
//    }
    cout << "All calculations finished." << endl;
    cin.get();
    return 1;
}
