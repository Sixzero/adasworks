#ifndef MYLAYER_H
#define MYLAYER_H

#include <Eigen/Dense>

class MyLayer
{
    MyLayer();
public:
    MyLayer(size_t s1, size_t s2);
    Eigen::MatrixXf myVal;
    Eigen::MatrixXf myGrad;
    Eigen::MatrixXf myErr;
    virtual void calcFP(MyLayer & inputLayer) = 0;
    virtual void calcBP(MyLayer &prevLayer, MyLayer &nextLayer) = 0;
    virtual void applyGrads(double lr) = 0;

};

#endif // MYLAYER_H
