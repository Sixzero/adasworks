#include "mysigmoidlayer.h"

#include <iostream>

MySigmoidLayer::MySigmoidLayer(size_t s1, size_t s2):
    MyLayer(s1, s2)
{
//    std::cout<<myWeights<<std::endl;
}
#include <math.h>
// define function to be applied coefficient-wise
float sigmoid(float x)
{
        return 1/(1+exp(-x));
}
void MySigmoidLayer::calcFP(MyLayer & inputLayer){
    myVal = inputLayer.myVal.unaryExpr(&sigmoid)  ;

}
float sigmoidDerivative(float x)
{
        return x/(1-x);
}
void MySigmoidLayer::calcBP(MyLayer & prevLayer, MyLayer &nextLayer){
    myGrad.fill(0.);


    myErr = (nextLayer.myErr.array() * myVal.unaryExpr(&sigmoidDerivative).array()).matrix();

}

void MySigmoidLayer::applyGrads(double lr)
{
}

