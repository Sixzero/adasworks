#ifndef MYMULLAYER_H
#define MYMULLAYER_H

#include <Eigen/Dense>
#include "mylayer.h"

class MyMulLayer : public MyLayer
{
    Eigen::MatrixXf myWeights;
public:
    MyMulLayer(size_t s1, size_t s2);


    void calcFP(MyLayer & inputLayer) override;
    void calcBP(MyLayer &prevLayer, MyLayer & nextLayer) override;
    void applyGrads(double lr) override;
};

#endif // MYMULLAYER_H
