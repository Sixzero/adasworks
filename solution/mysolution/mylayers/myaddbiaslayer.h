#ifndef MYADDBIASLAYER_H
#define MYADDBIASLAYER_H

#include <Eigen/Dense>
#include "mylayer.h"

class MyAddBiasLayer : public MyLayer
{
    Eigen::VectorXf myWeights;
public:
    MyAddBiasLayer(size_t s1, size_t s2);


    void calcFP(MyLayer & inputLayer) override;
    void calcBP(MyLayer &prevLayer, MyLayer & nextLayer) override;
    void applyGrads(double lr) override;
};

#endif // MYADDBIASLAYER_H
