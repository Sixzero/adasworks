#include "mylayer.h"

MyLayer::MyLayer():
    myVal(2,2), myGrad(2,2)
{

}

MyLayer::MyLayer(size_t s1, size_t s2):
    myVal(s1, s2), myGrad(s1, s2), myErr(s1, s2)
{

}
