#include "myaddbiaslayer.h"

#include <iostream>

MyAddBiasLayer::MyAddBiasLayer(size_t s1, size_t s2):
    myWeights(Eigen::VectorXf::Constant(s2,0.1)), MyLayer(s1, s2)
{
//    std::cout<<myWeights<<std::endl;
}
void MyAddBiasLayer::calcFP(MyLayer & inputLayer){

    myVal = inputLayer.myVal;
    myVal.rowwise() +=  myWeights.transpose();

}
void MyAddBiasLayer::calcBP(MyLayer & prevLayer, MyLayer &nextLayer){
    myGrad = nextLayer.myErr.colwise().sum();
    myErr = nextLayer.myErr ;

}

void MyAddBiasLayer::applyGrads(double lr)
{

    myWeights += myGrad.transpose() *lr;
}
