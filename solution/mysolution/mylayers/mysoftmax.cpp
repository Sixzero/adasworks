#include "mysoftmax.h"

#include <iostream>

MySoftmax::MySoftmax(size_t s1, size_t s2):
    MyLayer(s1, s2)
{
//    std::cout<<myWeights<<std::endl;
}
#include <math.h>
// define function to be applied coefficient-wise
float softmax(float x)
{
        return exp(x);
}
void MySoftmax::calcFP(MyLayer & inputLayer){
    Eigen::MatrixXf tmp = inputLayer.myVal.unaryExpr(&softmax);
    myVal = tmp/tmp.sum()  ;

}
float softmaxDerivative(float x)
{
        return exp(x);
}
void MySoftmax::calcBP(MyLayer & prevLayer, MyLayer &nextLayer){
    myGrad.fill(0.);


    myErr = (nextLayer.myErr.array() * myVal.unaryExpr(&softmaxDerivative).array()).matrix();

}

void MySoftmax::applyGrads(double lr)
{
}

