#ifndef MYINPUTLAYER_H
#define MYINPUTLAYER_H


#include <Eigen/Dense>
#include "mylayer.h"

class MyInputLayer : public MyLayer
{
public:
    Eigen::MatrixXf myInput;
    MyInputLayer(size_t s1, size_t s2);
    MyInputLayer(Eigen::MatrixXf &a);


    void calcFP(MyLayer & inputLayer) override;
    void calcBP(MyLayer & prevLayer, MyLayer &nextLayer) override;
    void applyGrads(double lr) override;
};


#endif // MYINPUTLAYER_H
