#include "myoutputlayer.h"
#include <iostream>

MyOutputLayer::MyOutputLayer(size_t s1, size_t s2):
    myRef(s1,s2), MyLayer(s1, s2)
{

}
MyOutputLayer::MyOutputLayer(Eigen::MatrixXf &a):
    myRef(a), MyLayer(a.rows(), a.cols())
{

}
void MyOutputLayer::calcFP(MyLayer & inputLayer){
    myVal = inputLayer.myVal ;

}

// define function to be applied coefficient-wise
float ramp(float x)
{
    if (x > 1)
        return 0.9+ 0.1*x;
    else if (x > 0)
        return x;
    else
        return 0.1 * x;
}
#include <math.h>
// define function to be applied coefficient-wise
float mylog(float x)
{
        return log(x);
}
float signekeeperSquare(float x)
{
        return x * abs(x);
}
void MyOutputLayer::calcBP(MyLayer & prevLayer, MyLayer &nextLayer){
    myGrad.fill(0.);
//    auto valCut = myVal.unaryExpr(&ramp);
//    auto tmpYes = (valCut.array() * myRef.array()).matrix();
//    auto tmpNo = (valCut.array() * (1-myRef.array())).matrix();
//    myErr = - tmpYes + tmpNo ;
//    myErr = (myRef - myVal.unaryExpr(&ramp)) .unaryExpr(&signekeeperSquare);
    myErr = (myRef - myVal.unaryExpr(&ramp)) ;
//    myErr = -(myRef.array() * myVal.unaryExpr(&mylog).array()).matrix();

}
void MyOutputLayer::applyGrads(double lr)
{
//    myErr.fill(0.);
}
#include <iostream>
float MyOutputLayer::getAccuracy() {
    float acc=0;
    for(int i=0; i<myRef.rows();++i) {
        int j=0;
        for(; j<myRef.cols(); ++j) {
            if (myRef(i, j)==1) {
                break;
            }
        }
        int h=0;
        float biggest=myVal(i, 0);
        for(int k=1; k<myRef.cols(); ++k) {
            if (myVal(i, k)>biggest) {
                h=k;
                biggest = myVal(i, k);
            }
        }
        if (h == j) {
            acc+=1;
        }
        else {
            misslabelledInputs.push_back(i);
            wrongLabels.push_back(h);
        }

    }
    return acc / float(myRef.rows());
}
