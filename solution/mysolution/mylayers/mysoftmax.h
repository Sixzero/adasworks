#ifndef MYSOFTMAX_H
#define MYSOFTMAX_H


#include <Eigen/Dense>
#include "mylayer.h"

class MySoftmax : public MyLayer
{
public:
    MySoftmax(size_t s1, size_t s2);


    void calcFP(MyLayer & inputLayer) override;
    void calcBP(MyLayer &prevLayer, MyLayer & nextLayer) override;
    void applyGrads(double lr) override;
};

#endif // MYSOFTMAX_H
