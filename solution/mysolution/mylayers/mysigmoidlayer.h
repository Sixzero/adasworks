#ifndef MYSIGMOIDLAYER_H
#define MYSIGMOIDLAYER_H


#include <Eigen/Dense>
#include "mylayer.h"

class MySigmoidLayer : public MyLayer
{
public:
    MySigmoidLayer(size_t s1, size_t s2);


    void calcFP(MyLayer & inputLayer) override;
    void calcBP(MyLayer &prevLayer, MyLayer & nextLayer) override;
    void applyGrads(double lr) override;
};

#endif // MYSIGMOIDLAYER_H
