#ifndef MYRELULAYER_H
#define MYRELULAYER_H

#include <Eigen/Dense>
#include "mylayer.h"

class MyReluLayer : public MyLayer
{
public:
    MyReluLayer(size_t s2);


    void calcFP(MyLayer & inputLayer) override;
    void calcBP(MyLayer &prevLayer, MyLayer & nextLayer) override;
    void applyGrads(double lr) override;
};


#endif // MYRELULAYER_H
