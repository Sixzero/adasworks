#ifndef MYOUTPUTLAYER_H
#define MYOUTPUTLAYER_H


#include <Eigen/Dense>
#include "mylayer.h"
#include <vector>

class MyOutputLayer : public MyLayer
{

public:
    std::vector<size_t> misslabelledInputs;
    std::vector<size_t> wrongLabels;
    Eigen::MatrixXf myRef;
    MyOutputLayer(size_t s1, size_t s2);
    MyOutputLayer(Eigen::MatrixXf &a);


    void calcFP(MyLayer & inputLayer) override;
    void calcBP(MyLayer &prevLayer, MyLayer & nextLayer) override;
    void applyGrads(double lr) override;
    float getAccuracy();
};


#endif // MYOUTPUTLAYER_H
