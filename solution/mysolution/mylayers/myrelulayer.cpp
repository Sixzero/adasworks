#include "myrelulayer.h"

#include <iostream>

MyReluLayer::MyReluLayer(size_t s2):
    MyLayer(1, s2)
{
//    std::cout<<myWeights<<std::endl;
}
#include <math.h>
// define function to be applied coefficient-wise
float relu(float x)
{
    if (x>=0) {
        return x;
    }
    else
        return 0.1 * x;
}
void MyReluLayer::calcFP(MyLayer & inputLayer){
    myVal = inputLayer.myVal.unaryExpr(&relu)  ;

}
float reluDerivative(float x)
{
    if (x>=0) {
        return 1.;
    }
    else
        return 0.1;
}
void MyReluLayer::calcBP(MyLayer & prevLayer, MyLayer &nextLayer){
    myGrad.fill(0.);
    myErr = (nextLayer.myErr.array() * myVal.unaryExpr(&reluDerivative).array()).matrix();

}

void MyReluLayer::applyGrads(double lr)
{
}
