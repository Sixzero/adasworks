#include "myinputlayer.h"


MyInputLayer::MyInputLayer(size_t s1, size_t s2):
    myInput(s1,s2), MyLayer(s1, s2)
{

}

MyInputLayer::MyInputLayer(Eigen::MatrixXf &a):
    myInput(a), MyLayer(a.rows(), a.cols())
{

}
void MyInputLayer::calcFP(MyLayer & inputLayer){
    myVal = myInput ;

}
void MyInputLayer::calcBP(MyLayer &prevLayer, MyLayer & nextLayer){
    myGrad.fill(0.);
    myErr.fill(0.);

}

void MyInputLayer::applyGrads(double lr)
{

}
