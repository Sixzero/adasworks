#include "mymullayer.h"

#include <iostream>

MyMulLayer::MyMulLayer(size_t s1, size_t s2):
    myWeights(Eigen::MatrixXf::Random(s1,s2)/10.), MyLayer(s1, s2)
{
//    std::cout<<myWeights<<std::endl;
}
void MyMulLayer::calcFP(MyLayer & inputLayer){
    myVal = inputLayer.myVal * myWeights;

}
void MyMulLayer::calcBP(MyLayer & prevLayer, MyLayer &nextLayer){
    myGrad = prevLayer.myVal.transpose() * nextLayer.myErr;
    myErr = nextLayer.myErr * myWeights.transpose();

}

void MyMulLayer::applyGrads(double lr)
{
    myWeights += myGrad * lr;
}
