#ifndef MYMODELCONTAINER_H
#define MYMODELCONTAINER_H

#include <vector>
#include "mylayers/mylayer.h"

class MyModelContainer
{
    void calcFP() ;
    void calcBP() ;
    void applyGrads(float lr) ;
public:
    std::vector<MyLayer*> myLayers;
    MyModelContainer();
    void run(float lr) ;
    void predict();
};

#endif // MYMODELCONTAINER_H
