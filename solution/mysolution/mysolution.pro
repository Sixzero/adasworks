QT += core
QT -= gui

CONFIG += c++11

TARGET = mysolution
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    mymatrix.cpp \
    mymodelcontainer.cpp \
    mydatasetreader.cpp \
    mylayers/myaddbiaslayer.cpp \
    mylayers/myinputlayer.cpp \
    mylayers/mylayer.cpp \
    mylayers/mymullayer.cpp \
    mylayers/myoutputlayer.cpp \
    mylayers/myrelulayer.cpp \
    mylayers/mysigmoidlayer.cpp \
    mylayers/mysoftmax.cpp

HEADERS += \
    mymatrix.h \
    mymodelcontainer.h \
    mydatasetreader.h \
    justforfun.h \
    mylayers/myaddbiaslayer.h \
    mylayers/myinputlayer.h \
    mylayers/mylayer.h \
    mylayers/mymullayer.h \
    mylayers/myoutputlayer.h \
    mylayers/myrelulayer.h \
    mylayers/mysigmoidlayer.h \
    mylayers/mysoftmax.h

INCLUDEPATH += /usr/include/eigen3
QMAKE_CXXFLAGS += -O3
#QMAKE_CXXFLAGS += -fopenmp # több mag kihasználásához....
