#include "mymodelcontainer.h"

MyModelContainer::MyModelContainer()
{

}

void MyModelContainer::predict() {
    calcFP();
}

void MyModelContainer::run(float lr)
{
    calcFP();
    calcBP();
    applyGrads(lr);
}

void MyModelContainer::calcFP()
{

    for (int i=0; i<myLayers.size(); ++i) {
        if (i==0) {
            myLayers[i]->calcFP(*myLayers[i]); // nem kéne beadni semmit se... de lényegtelen...
        } else {
            myLayers[i]->calcFP(*myLayers[i-1]);
        }
    }
}

void MyModelContainer::calcBP()
{
    for (int i=myLayers.size()-1; i>=0; --i) {
        if (i==0) {
            myLayers[i]->calcBP(*myLayers[i], *myLayers[i+1]); // nem kéne beadni semmit se... de lényegtelen...
        } else if (i+1==myLayers.size()) {
            myLayers[i]->calcBP(*myLayers[i-1], *myLayers[i]);
        } else {
            myLayers[i]->calcBP(*myLayers[i-1], *myLayers[i+1]);
        }
    }
}

void MyModelContainer::applyGrads(float lr)
{
    for (int i=0; i<myLayers.size(); ++i) {
        MyLayer* l = myLayers[i];
        l->applyGrads(lr);
    }
}
